CHANGELOG

Current
* Feature - Added sample code to demonstrate method for using tab or accordion layouts with modal content.

7.x-1.0-Beta3
* Bug - Contacts now open in modal frame when viewing, editing or deleting from within organizations and event tab.
* Bug - Can now import CSV files containing multibyte characters. CSV file must be encoded as UTF-8. Relevant UTF-8 detection methods have been included as part of file upload validation.
* Bug - Linked nodes will not be removed if they are still linked to one or more contacts.
* Bug - Autocomplete text field results remain in correct position after modal frame content has been scrolled.
* Feature - Added automated tests. 
* Bug - Issues with setting and getting namecards user settings if user has "namecards administrators" but not "namecards access" permission set. 

7.x-1.0-Beta2
* Bug - Form event field now accepts non-English characters.

7.x-1.0-Beta1