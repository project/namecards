<?php

/**
 * Implements hook_install()
 */
function namecards_install() {
  // Create settings for super user (uid = 1)
  $fields = array(
    'uid' => 1,
    'default_page' => 0,
    'browse_surname' => 1,
    'browse_given_name' => 1,
    'browse_nickname' => 1,
    'browse_organization' => 1,
    'browse_position' => 1,
    'browse_department' => 0,
    'browse_email' => 1,
    'browse_mobile' => 1,
    'browse_phone' => 1,
    'browse_fax' => 1,
    'browse_address' => 0,
    'browse_event' => 0,
    'contact_by_org_surname' => 1,
    'contact_by_org_given_name' => 1,
    'contact_by_org_nickname' => 1,
    'contact_by_org_organization' => 1,
    'contact_by_org_position' => 1,
    'contact_by_org_department' => 0,
    'contact_by_org_email' => 1,
    'contact_by_org_mobile' => 1,
    'contact_by_org_phone' => 1,
    'contact_by_org_fax' => 1,
    'contact_by_org_address' => 0,
    'contact_by_org_event' => 0,
    'contact_by_event_surname' => 1,
    'contact_by_event_given_name' => 1,
    'contact_by_event_nickname' => 1,
    'contact_by_event_organization' => 1,
    'contact_by_event_position' => 1,
    'contact_by_event_department' => 0,
    'contact_by_event_email' => 1,
    'contact_by_event_mobile' => 1,
    'contact_by_event_phone' => 1,
    'contact_by_event_fax' => 1,
    'contact_by_event_address' => 0,
    'contact_by_event_event' => 0,
  );
  db_insert('user_namecard_settings')->fields($fields)->execute();
  drupal_set_message('Table \'user_namecard_settings\' has been created');

  // Set any namecards module specific settings variables.
  variable_set('namecards_privacy_settings_options', 0);
  variable_set('namecards_delete_user_contacts_options', 0);
}

/**
 * Implements hook_uninstall()
 */
function namecards_uninstall() {

  // Delete all already existing nodes belonging to the above node types.
  $results = db_query('SELECT nid FROM {node} WHERE type RLIKE :pattern', array(':pattern' => 'namecards_[a-zA-Z]{1,}'));
  foreach ($results as $result) {
    node_delete($result->nid);
  }

  // Remove any namecards module specific variables from variable table in database.
  variable_del('namecards_privacy_settings_options');
  variable_del('namecards_delete_user_contacts_options');
  variable_del('namecards_caching_settings_options');
  variable_del('namecards_compression_settings_options');
  

  menu_rebuild();
}

/**
 * Implements hook_schema()
 */
function namecards_schema() {
  $schema['user_namecard_settings'] = array(
    'description' => t('Stores custom UI setting information for a given user'),
    'fields' => array(
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('The uid is the id of the user to which these settings belong to.'),
      ),
      'default_page' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1, // 0 = 'browse', 1 = 'organizations', 2 = 'events'
        'description' => t('The default page shown when user logins in.'),
      ),
      'browse_surname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_given_name' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_nickname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_organization' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_position' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_department' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_email' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_mobile' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_phone' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_fax' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_address' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'browse_event' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Browse Contacts view.'),
      ),
      'contact_by_org_surname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_given_name' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_nickname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_organization' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_position' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_department' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_email' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_mobile' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_phone' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_fax' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_address' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_org_event' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By Org view.'),
      ),
      'contact_by_event_surname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_given_name' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_nickname' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_organization' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_position' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_department' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_email' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_mobile' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_phone' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_fax' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 1,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_address' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      'contact_by_event_event' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Sets whether field is included in Contacts By event view.'),
      ),
      ),
    'primary key' => array('uid'),
  );
  $schema['cache_namecards'] = array(
    'description' => t('Stores cached views for individual users.'),
    'fields' => array(
      'cid' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => t('A unique string identifying the cached data.'),
      ),
      'data' => array(
        'mysql_type' => 'LONGBLOB',
        'type' => 'blob',
        'size' => 'big',
        'not null' => FALSE,
        'default' => NULL,
        'description' => t('Contains the cached data.'),
      ),
      'expire' => array(
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'description' => t(''),
      ),
      'created' => array(
        'type' => 'int',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'description' => t(''),
      ),
      'headers' => array(
        'type' => 'text',
        'size' => 'normal',
        'not null' => FALSE,
        'description' => t(''),
      ),
      'serialized' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => TRUE,
        'default' => 0,
        'description' => t('Indicates whether cached data is serialized or not.'),
      ),
    ),
    'primary key' => array('cid'),
    'indexes' => array(
      'expire' => array('expire'),
    ),
  );

  return $schema;
}