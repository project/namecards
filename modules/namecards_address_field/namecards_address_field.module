<?php
/**
 * @file
 * An address field created using the Field API.
 *
 */

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function namecards_address_field_field_info() {
  return array(
    'namecards_address_field_address' => array(
      'label' => t('Address (Namecards)'),
      'description' => t('The address of a contact.'),
      'default_widget' => 'namecards_address_field_address_widget',
      'default_formatter' => 'namecards_address_field_address_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info()
 */
function namecards_address_field_field_widget_info() {
  return array(
    'namecards_address_field_address_widget' => array(
      'label' => t('Address'),
      'field types' => array('namecards_address_field_address'),
    ),
  );
}

/**
 * Implements hook_field_widget_form()
 */
function namecards_address_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Mkae top level element a fieldset.
  $element += array(
    '#type' => 'fieldset',
  );

//  $required = $element['#required'];
  $item =& $items[$delta];

  $element['street'] = array(
    '#title' => t('Street'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['street']) ? $item['street'] : '',
  );
  $element['district'] = array(
    '#title' => t('District/Suburb'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['district']) ? $item['district'] : '',
  );
  $element['city'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['city']) ? $item['city'] : '',
  );
  $element['state'] = array(
    '#title' => t('State'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['state']) ? $item['state'] : '',
  );
  $element['postcode'] = array(
    '#title' => t('Postcode'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['postcode']) ? $item['postcode'] : '',
  );
  $element['country'] = array(
    '#title' => t('Country'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['country']) ? $item['country'] : '',
  );

  return $element;
}

/**
 * Implements hook_field_is_empty()
 */
function namecards_address_field_field_is_empty($item, $field) {
  $is_empty = FALSE;
  if (empty($item['street']) && empty($item['district']) && empty($item['city']) && empty($item['state']) && empty($item['postcode']) && empty($item['country'])) {
    $is_empty =  TRUE; // Will not write address field to database if all elements are empty.
  }
  return $is_empty;
}

/**
 * Implements hook_field_schema()
 */
function namecards_address_field_field_schema($field) {
  $columns = array(
    'street' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
    ),
    'district' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
    'city' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
    'state' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
    'postcode' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
    'country' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ),
  );

  return array(
    'columns' => $columns,
    'indexes' => array(),
  );
}

/**
 * Implements hook_field_validate()
 */
function namecards_address_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // No validation performed.  Is left here as a reminder
  // should I be tempted to clone this module later.
}

function namecards_address_field_field_widget_error($element, $error, $form, &$form_state) {
  // This function is not used.  Is left here as a reminder
  // should I be tempted to clone this module later.
}

/**
 * Implements hook_field_formatter_info()
 */
function namecards_address_field_field_formatter_info() {
  return array(
    'namecards_address_field_address_formatter' => array(
      'label' => t('Default'),
      'field types' => array('namecards_address_field'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view()
 */
function namecards_address_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {
    $element[$delta] = namecards_address_field_format_field($item);
  }
  return $element;
}

/**
 * Implements hook_format_field()
 */
function namecards_address_field_format_field($item) {
  $element = array(
    '#type' => 'container',
    '#attributes' => array( 'class' => array( 'field-item') ),
  );
  $element['street'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('Street'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['street'],
      ),
    ),
  );
  $element['district'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('District'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['district'],
      ),
    ),
  );
  $element['city'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('City'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['city'],
      ),
    ),
  );
  $element['state'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('State'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['state'],
      ),
    ),
  );
  $element['postcode'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('Postcode'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['postcode'],
      ),
    ),
  );
  $element['country'] = array(
    'label' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-label' )),
      'text' => array(
        '#markup' => t('Country'),
      ),
    ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['country'],
      ),
    ),
  );
  return $element;
}