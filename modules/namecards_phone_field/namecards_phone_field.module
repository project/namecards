<?php
/**
 * @file
 * An phone field created using the Field API.
 *
 */

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function namecards_phone_field_field_info() {
  return array(
    'namecards_phone_field_phone' => array(
      'label' => t('Phone (Namecards)'),
      'description' => t('The phone number of a contact.'),
      'default_widget' => 'namecards_phone_field_phone_widget',
      'default_formatter' => 'namecards_phone_field_phone_formatter',
    ),
  );
}

/**
 * Implements hook_field_widget_info()
 */
function namecards_phone_field_field_widget_info() {
  return array(
    'namecards_phone_field_phone_widget' => array(
      'label' => t('Phone (Namecards)'),
      'field types' => array('namecards_phone_field_phone'),
    ),
  );
}

/**
 * Implements hook_field_widget_form()
 */
function namecards_phone_field_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Make top level element a fieldset.
  $element += array(
    '#type' => 'fieldset',
  );

//  $required = $element['#required'];
  $item =& $items[$delta];

  $element['phone_number'] = array(
    '#title' => t('Phone'),
    '#type' => 'textfield',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#default_value' => isset($item['phone_number']) ? $item['phone_number'] : '',
  );
  $element['phone_type'] = array(
    '#title' => t('type'),
    '#type' => 'select',
    '#required' => FALSE,
    // use #default_value to prepopulate the element
    // with the current saved value
    '#options' => array(
      0 => t('Work'),
      1 => t('Home'),
      2 => t('Other'),
    ),
    '#default_value' => isset($item['phone_type']) ? $item['phone_type'] : 0,
  );

  return $element;
}

/**
 * Implements hook_field_is_empty()
 */
function namecards_phone_field_field_is_empty($item, $field) {
  $is_empty = FALSE;
  if (empty($item['phone_number'])) {
    $is_empty =  TRUE; // Will not write phone field to database if all elements are empty.
  }
  return $is_empty;
}

/**
 * Implements hook_field_schema()
 */
function namecards_phone_field_field_schema($field) {
  $columns = array(
    'phone_number' => array(
      'type' => 'varchar',
      'length' => 255,
      'not null' => TRUE,
    ),
    'phone_type' => array(
      'type' => 'int',
      'size' => 'tiny',
      'not null' => FALSE,
      'description' => t('Phone type.'),
    ),
  );

  return array(
    'columns' => $columns,
    'indexes' => array(),
  );
}

/**
 * Implements hook_field_validate()
 */
function namecards_phone_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  // No validation performed.  Is left here as a reminder
  // should I be tempted to clone this module later.
}

function namecards_phone_field_field_widget_error($element, $error, $form, &$form_state) {
  // This function is not used.  Is left here as a reminder
  // should I be tempted to clone this module later.
}

/**
 * Implements hook_field_formatter_info()
 */
function namecards_phone_field_field_formatter_info() {
  return array(
    'namecards_phone_field_phone_formatter' => array(
      'label' => t('Default'),
      'field types' => array('namecards_phone_field'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view()
 */
function namecards_phone_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  foreach ($items as $delta => $item) {
    $element[$delta] = namecards_phone_field_format_field($item);
  }
  return $element;
}

/**
 * Implements hook_format_field()
 */
function namecards_phone_field_format_field($item) {
  $element = array(
    '#type' => 'container',
    '#attributes' => array( 
      'class' => array('field-item'),
    ),
  );
  $element['phone_number'] = array(
//     'label' => array(
//       '#type' => 'container',
//       '#attributes' => array(
//         'class' => array('field-label'),
//       ),
//       'text' => array(
//         '#markup' => t('Phone number'),
//       ),
//     ),
    'item' => array(
      '#type' => 'container',
      '#attributes' => array( 'class' => array( 'field-item') ),
      'text' => array(
        '#markup' => $item['phone_number'],
      ),
    ),
  );
//   $element['phone_type'] = array(
//     'label' => array(
//       '#type' => 'container',
//       '#attributes' => array( 'class' => array( 'field-label' )),
//       'text' => array(
//         '#markup' => t('Phone type'),
//       ),
//     ),
//     'item' => array(
//       '#type' => 'container',
//       '#attributes' => array( 'class' => array( 'field-item') ),
//       'text' => array(
//         '#markup' => $item['phone_type'],
//       ),
//     ),
//   );

  return $element;
}