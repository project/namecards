<?php

/**
 * Implements hook_init();
 */
function namecards_tabbed_layout_init() {
  // Add javascript to create tabbed layout.
  theme('namecards_tabbed_layout_js');
}

/*
 * Implements hook_form_alter()
 *
 * Re-arranges form fields and adds additional html elements to create a
 * structure compatible for use with jQuery UI Tabs widget.
 */
function namecards_tabbed_layout_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'namecards_namecard_node_form') {
    // Appears that the title fields weight cannot be altered, so must determine
    // its current weight and arrange all other fields before or after it
    // accordingly.
    $title_weight = $form['title']['#weight'];
    $form['tabs_wrapper_start'] = array(
      '#markup' => '<div id="tabs">',
      '#weight' => $title_weight - 3,
    );
    $form['tabs_menu'] = array(
      '#markup' => '<ul><li><a href="#tab-0">Info</a></li><li><a href="#tab-1">Contact</a></li><li><a href="#tab-2">Other</a></li></ul>',
      '#weight' => $title_weight - 2,
    );
    $form['tab0_start'] = array(
      '#markup' => '<div id="tab-0">',
      '#weight' => $title_weight - 1,
    );
    $form['title']['#weight'] = $title_weight;
    // Everything after the title has a non-negative weight.  No reason for this
    // other than don't like dealing with negative values. Non-negative values
    // seem cleaner.
    $weight = ($title_weight < 0) ? 0 : $title_weight;
    $form['namecards_namecard_given_name']['#weight'] = $weight++;
    $form['namecards_namecard_nickname']['#weight'] = $weight++;
    $form['namecards_namecard_position']['#weight'] = $weight++;
    $form['namecards_namecard_department']['#weight'] = $weight++;
    $form['namecards_namecard_organization']['#weight'] = $weight++;
    $form['tab0_end'] = array(
      '#markup' => '</div>',
      '#weight' => $weight++,
    );
    $form['tab1_start'] = array(
      '#markup' => '<div id="tab-1">',
      '#weight' => $weight++,
    );
    $form['namecards_namecard_email']['#weight'] = $weight++;
    $form['namecards_namecard_phone']['#weight'] = $weight++;
    $form['namecards_namecard_mobile']['#weight'] = $weight++;
    $form['namecards_namecard_fax']['#weight'] = $weight++;
    $form['namecards_namecard_address']['#weight'] = $weight++;
    $form['tab1_end'] = array(
      '#markup' => '</div>',
      '#weight' => $weight++,
    );
    $form['tab2_start'] = array(
      '#markup' => '<div id="tab-2">',
      '#weight' => $weight++,
    );
    $form['namecards_namecard_public']['#weight'] = $weight++;
    $form['namecards_namecard_event']['#weight'] = $weight++;
    $form['namecards_namecard_notes']['#weight'] = $weight++;
    $form['namecards_namecard_full_content']['#weight'] = $weight++;
    $form['namecards_namecard_comp_email']['#weight'] = $weight++;
    $form['namecards_namecard_comp_org']['#weight'] = $weight++;
    $form['namecards_namecard_comp_pos']['#weight'] = $weight++;
    $form['namecards_namecard_comp_event']['#weight'] = $weight++;
    $form['namecards_namecard_comp_department']['#weight'] = $weight++;
    $form['tab2_end'] = array(
      '#markup' => '</div>',
      '#weight' => $weight++,
    );
    $form['tabs_wrapper_end'] = array(
      '#markup' => '</div>',
      '#weight' => $weight++,
    );
  }
}

/**
 * Implements hook_theme()
 */
function namecards_tabbed_layout_theme($existing, $type, $theme, $path) {
  return array(
    'namecards_tabbed_layout_js' => array(
      'variables' => array(),
    )
  );
}

/**
 * Add required javascript to page
 */
function theme_namecards_tabbed_layout_js($variables) {
  // Add required files.
  drupal_add_library('system', 'ui.tabs');
  drupal_add_js(drupal_get_path('module', 'namecards_tabbed_layout') . '/namecards_tabbed_layout.js');
}