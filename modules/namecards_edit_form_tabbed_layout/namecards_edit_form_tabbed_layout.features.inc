<?php
/**
 * @file
 * namecards_edit_form_tabbed_layout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function namecards_edit_form_tabbed_layout_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
