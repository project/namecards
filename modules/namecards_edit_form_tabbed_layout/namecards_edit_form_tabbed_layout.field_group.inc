<?php
/**
 * @file
 * namecards_edit_form_tabbed_layout.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function namecards_edit_form_tabbed_layout_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_namecard_contact_tab|node|namecards_namecard|form';
  $field_group->group_name = 'group_namecard_contact_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'namecards_namecard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_namecard_edit_form_tabs';
  $field_group->data = array(
    'label' => 'Contact',
    'weight' => '49',
    'children' => array(
      0 => 'namecards_namecard_phone',
      1 => 'namecards_namecard_fax',
      2 => 'namecards_namecard_mobile',
      3 => 'namecards_namecard_email',
      4 => 'namecards_namecard_address',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-namecard-contact-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_namecard_contact_tab|node|namecards_namecard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_namecard_edit_form_tabs|node|namecards_namecard|form';
  $field_group->group_name = 'group_namecard_edit_form_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'namecards_namecard';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Tabs',
    'weight' => '0',
    'children' => array(
      0 => 'group_namecard_info_tab',
      1 => 'group_namecard_contact_tab',
      2 => 'group_namecard_other_tab',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-namecard-edit-form-tabs field-group-htabs ',
      ),
    ),
  );
  $export['group_namecard_edit_form_tabs|node|namecards_namecard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_namecard_info_tab|node|namecards_namecard|form';
  $field_group->group_name = 'group_namecard_info_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'namecards_namecard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_namecard_edit_form_tabs';
  $field_group->data = array(
    'label' => 'Info',
    'weight' => '48',
    'children' => array(
      0 => 'namecards_namecard_given_name',
      1 => 'namecards_namecard_nickname',
      2 => 'namecards_namecard_organization',
      3 => 'namecards_namecard_position',
      4 => 'namecards_namecard_department',
      5 => 'title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-namecard-info-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_namecard_info_tab|node|namecards_namecard|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_namecard_other_tab|node|namecards_namecard|form';
  $field_group->group_name = 'group_namecard_other_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'namecards_namecard';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_namecard_edit_form_tabs';
  $field_group->data = array(
    'label' => 'Other',
    'weight' => '50',
    'children' => array(
      0 => 'namecards_namecard_public',
      1 => 'namecards_namecard_event',
      2 => 'namecards_namecard_notes',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-namecard-other-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_namecard_other_tab|node|namecards_namecard|form'] = $field_group;

  return $export;
}
