/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {

  /*
   * Tab layout constructor class
   */
  function NamecardsTabLayout() {
    this.$wrapper = $('<div id="tabs"><ul></ul></div>');

    /*
     * Creates and adds a tab to the tab wrapper.
     */ 
    this.addTab = function(index, title) {
      var $tab = $('<li></li>');
      $tab.html('<a href="#tabs-' + index + '">' + Drupal.t(title) + '</a>');

      var $content = $('<div></div>');
      $content
        .attr('id', 'tabs-' + index);

      // Append tab to wrapper.
      this.$wrapper.children('ul').append($tab);
      this.$wrapper.append($content);
    };
    
    /*
     * Add content to a tab
     * 
     * @param Int index
     *   index of the target tab.
     *   
     * @param content
     *   Accepts a string of HTML or a jQuery object. Strings will be 
     *   automatically converted to a jQuery object.
     */
    this.addTabContent = function(index, content) {
      // Confirm index exists.
      $tab = $('#tabs-' + index, this.$wrapper);
      if ($tab.length < 1) {
        throw error = new Error('Tab index ' + index + ' does not exist.');
      }
      if (!content instanceof jQuery) {
        content = $(content);
      }
      $tab.append(content);
    };

    /*
     * Returns rendered tabs element.
     */
    this.theme = function() {
      // Initialize tabs.
      this.$wrapper.tabs({ event: 'mouseover' });
      
      // Return formatted object.
      return this.$wrapper;
    };
  } 

  /*
   * Accordion layout constructor class
   */
  function NamecardsAccordionLayout() {
    this.$wrapper = $('<div id="accordion"></div>');

    /*
     * Creates and adds a tab to the tab wrapper.
     */ 
    this.addSection = function(index, title) {
      var $accordionSectionHeader = $('<h3><a href="#">' + Drupal.t(title) + '</a></h3>');

      var $accordionSectionContent = $('<div></div>');
      $accordionSectionContent.attr('id', 'accordion-section-' + index);

      // Append tab to wrapper.
      this.$wrapper.append($accordionSectionHeader, $accordionSectionContent);
    };
    
    /*
     * Add content to a tab
     * 
     * @param Int index
     *   index of the target tab.
     *   
     * @param Array selectors
     *   jQuery selectors which will be appended to the tab.
     */
    this.addSectionContent = function(index, content) {
      // Confirm index exists.
      if ($('#accordion-section-' + index, this.$wrapper).length < 1) {
        throw error = new Error('Section index ' + index + ' does not exist.');
      }
      if (!content instanceof jQuery) {
        content = $(content);
      }
      $('#accordion-section-' + index, this.$wrapper).append(content);;
    };

    /*
     * Resize accordion
     */
    this.resize = function() {
      this.$wrapper.accordion('resize');
    };

    /*
     * Returns rendered tabs element.
     * 
     * @return Object
     *   Rendered accordion as jQuery object.
     */
    this.theme = function() {
      // Initialize accordion.
      this.$wrapper.accordion({
        autoHeight: false,
        collapsible: true
      });
      
      // Return formatted object.
      return this.$wrapper;
    };
  } 

  /*
   * Create table to insert into modal
   * 
   * @param Array
   *   selectors for content to be inserted into table.
   * @return Object
   *   jQuery object containing table.
   */
  var createTable = function(selectors) {
    var $table = $('<table></table>');
    var num = selectors.length;
    
    // Build table content.
    for (var $i = 0; $i < num; $i++) {
      $table.append($(selectors[$i]).parents('tr'));
    }
    
    // Redo table zebra striping.
    $('tr:odd', $table).removeClass('even').addClass('odd');
    $('tr:even', $table).removeClass('odd').addClass('even');
      
    return $table;
  };

  /*
   * Create contact formatted for inclusion in accordion layout when viewing 
   * edit form in modal
   * 
   * @param Array
   *   List of selectors.
   * @return String
   *   formatted HTML.
   */
  var createAccordionContent = function(selectors) {
    var num = selectors.length;
    var $content = $('<div></div>');
    
    // Build table content.
    for (var $i = 0; $i < num; $i++) {
      $content.append($(selectors[$i]));
    }
    
    return $content.html();
  };

  Drupal.behaviors.namecardsCtoolsModalTabs = {
    attach: function (context, settings) {
      // Convert details modal content to tab layout.
      if ($('.namecards-contact-details-table').length !== 0) {
        var $modalContent = $('#modal-content');
        var tabLayout = new NamecardsTabLayout();
        // Create tabs.
        tabLayout.addTab(0, Drupal.t('Personal'));
        tabLayout.addTab(1, Drupal.t('Contact'));
        tabLayout.addTab(2, Drupal.t('Other'));

        // Add contents to 'Personal' tab.
        var selectors = [
          '.namecards-table-cell-surname',
          '.namecards-table-cell-given-name',
          '.namecards-table-cell-nickname',
          '.namecards-table-cell-position',
          '.namecards-table-cell-department',
          '.namecards-table-cell-organization'
        ];
        var $table = createTable(selectors);
        tabLayout.addTabContent(0, $table);

        // Add contents to 'Contact' tab.
        var selectors = [
          '.namecards-table-cell-phone',
          '.namecards-table-cell-fax',
          '.namecards-table-cell-mobile',
          '.namecards-table-cell-email',
          '.namecards-table-cell-address'
        ];
        var $table = createTable(selectors);
        tabLayout.addTabContent(1, $table);

        // Add contents to 'Other' tab.
        var selectors = [
          '.namecards-table-cell-public',
          '.namecards-table-cell-event',
          '.namecards-table-cell-notes'
        ];
        var $table = createTable(selectors);
        tabLayout.addTabContent(2, $table);

        // Attach rendered tabs to page.
        $modalContent.html(tabLayout.theme());
      }
      
      // Convert details modal content to tab layout.
      if ($('#modal-content #namecards-namecard-node-form').length !== 0) {
        var $modalContent = $('#modal-content');
        var accordionLayout = new NamecardsAccordionLayout();
        
        accordionLayout.addSection(0, 'Personal');
        var content = createAccordionContent([
          '.form-item-title', // Surname
          '#edit-namecards-namecard-given-name',
          '#edit-namecards-namecard-nickname',
          '#edit-namecards-namecard-position',
          '#edit-namecards-namecard-department',
          '#edit-namecards-namecard-organization'
        ]);
        accordionLayout.addSectionContent(0, content);
        
        accordionLayout.addSection(1, 'Contact');
        var content = createAccordionContent([
          '#edit-namecards-namecard-phone', 
          '#edit-namecards-namecard-mobile',
          '#edit-namecards-namecard-email',
          '#edit-namecards-namecard-fax',
          '#edit-namecards-namecard-address'
        ]);
        accordionLayout.addSectionContent(1, content);
        
        accordionLayout.addSection(2, 'Other');
        var content = createAccordionContent([
          '#edit-namecards-namecard-public', 
          '#edit-namecards-namecard-event',
          '#edit-namecards-namecard-notes'
        ]);
        accordionLayout.addSectionContent(2, content);
        
        // Attach rendered tabs to page.
        $('#namecards-namecard-node-form').prepend(accordionLayout.theme());
        accordionLayout.resize();
      }
    }
  };

}(jQuery));
