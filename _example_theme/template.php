<?php

/**
 * Modify themeing of view details in modal.
 */

// Add required files.
drupal_add_library('system', 'ui.tabs');
drupal_add_library('system', 'ui.accordion');
drupal_add_js(drupal_get_path('theme', 'mytheme') . '/viewDetailsInModal.js');