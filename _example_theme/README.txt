Due to a request for information on how to theme content within the 'details' 
and 'edit' modals, I have put together this package.  This is intended to 
demonstrate how modal content layout could be altered to make use of jQuery UI 
tabs or accordion effects. 

You can use the existing theme layer to theme the modal content of the 
namecards module.  To run this demonstration you should add the content of 
'template.php' file contained in this example into your own themes 'template.php' 
file.  Done for get to change the name "mytheme" to that of your theme. You 
will also need to copy the file 'viewDetailsInModal.js' to your theme's 
directory.

WARNING: DON'T REPLACE YOUR THEME'S template.php FILE WITH THIS ONE, 
OTHERWISE IT WILL SCREW EVERYTHING UP MAJORLY. YOU ONLY NEED TO ADD THE 
CONTENT OF THIS FILE TO YOUR EXISTING FILE.