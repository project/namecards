THIS FILE CONTAINS SPECIFIC NOTES PERTAINING TO THE INSTALLATION AND USE OF THE 
'namecards' MODULE

TABBED LAYOUT FOR NAMECARD ADD/EDIT FORM

One can change the default layout of the namecard add/edit form to make use of 
tabs. This is implemented via the features module.  To use this feature simply 
enable the "Namecards Edit Form Tabbed Layout" module in "Namecards" section of 
the modules page. IMPORTANT NOTE: there is a bug in the current 7.x-1.1 version 
of the fieldgroup module which prevents it working correctly with features.  
This has been fixed in the current 7.x-1.x-dev version of fieldgroup.